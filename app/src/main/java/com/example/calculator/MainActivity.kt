package com.example.calculator

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnFrameLayout = findViewById<Button>(R.id.btnFrameLayout)
        val btnLinearLayout = findViewById<Button>(R.id.btnLinearLayout)
        val btnConstraintLayout = findViewById<Button>(R.id.btnConstraintLayout)

        btnFrameLayout.setOnClickListener {
            startActivity(Intent(this, CalculatorWithFrameLayoutActivity::class.java))
        }

        btnLinearLayout.setOnClickListener {
            startActivity(Intent(this, CalculatorWithLinearLayoutActivity::class.java))
        }

        btnConstraintLayout.setOnClickListener {
            startActivity(Intent(this, CalculatorWithConstraintLayoutActivity::class.java))
        }
    }
}
