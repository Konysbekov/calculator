package com.example.calculator

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class CalculatorWithConstraintLayoutActivity : AppCompatActivity() {

    private lateinit var screen: TextView
    private var displayText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator_constraint_layout)

        screen = findViewById(R.id.screen)

        val buttonIds = listOf(
            R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6,
            R.id.button7, R.id.button8, R.id.button9, R.id.buttonAdd, R.id.buttonSubtract, R.id.buttonMultiply
        )

        buttonIds.forEach { id ->
            findViewById<Button>(id).setOnClickListener {
                handleButtonClick(it as Button)
            }
        }
    }

    private fun handleButtonClick(button: Button) {
        val text = button.text.toString()
        displayText += text
        screen.text = displayText
    }
}
